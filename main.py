from collections import defaultdict

import matplotlib
import matplotlib.pyplot as plt
import networkx as nx
import string
import random


############### Algoritmo do 3-opt ###################

def distance(i, j):
	if i >= j:
		return i - j
	if j > i:
		return j - i

def reverse_segment_if_better(tour, i, j, k):
	A, B, C, D, E, F = tour[i-1], tour[i], tour[j-1], tour[j], tour[k-1], tour[k % len(tour)]
	d0 = distance(A, B) + distance(C, D) + distance(E, F)
	d1 = distance(A, C) + distance(B, D) + distance(E, F)
	d2 = distance(A, B) + distance(C, E) + distance(D, F)
	d3 = distance(A, D) + distance(E, B) + distance(C, F)
	d4 = distance(F, B) + distance(C, D) + distance(E, A)

	if d0 > d1:
		tour[i:j] = reversed(tour[i:j])
		return -d0 + d1
	elif d0 > d2:
		tour[j:k] = reversed(tour[j:k])
		return -d0 + d2
	elif d0 > d4:
		tour[i:k] = reversed(tour[i:k])
		return -d0 + d4
	elif d0 > d3:
		tmp = tour[j:k] + tour[i:j]
		tour[i:k] = tmp
		return -d0 + d3
	return 0

def three_opt(tour):
	"""Iterative improvement based on 3 exchange."""
	while True:
		delta = 0
		for (a, b, c) in all_segments(len(tour)):
			delta += reverse_segment_if_better(tour, a, b, c)
		if delta >= 0:
			break
	return tour

def all_segments(n: int):
	"""Generate all segments combinations"""
	return ((i, j, k)
		for i in range(n)
		for j in range(i + 2, n)
		for k in range(j + 2, n + (i > 0)))
	
############### Algoritmo do 3-opt ###################

#desenhando grafo com a lib networkx
def drawing(graph):

	G1 = nx.Graph()
	#graph no formato vertice-vertice-peso ('u', 'v', 1)
	for i in graph:
		G1.add_weighted_edges_from(graph[i])

	G = nx.eulerize(G1)
	position = nx.spring_layout(G)

	nx.draw(G, position, with_labels=True)
	weights = nx.get_edge_attributes(G, 'peso')
	nx.draw_networkx_edge_labels(G, position, edge_labels=weights)

	plt.show()


#recebo vetor de arestas e crio a rota com os vertices
def create_tour(edges):
	vertices = []
	for i in range(0, len(edges)):
		v = (random.choice(string.ascii_uppercase))
		vertices.append(v)

	vertices.append(vertices[1])

	graph = {}
	for i in range(0, len(edges)):
		graph.setdefault(i, []).append((vertices[i], vertices[i+1], edges[i]))
	
	return graph



if __name__ == "__main__":
	
	arestas = []
	tam = int(input("Entre com o tamanho da rota: "))
	for i in range(0, tam):
		aresta = int(input("entre com o peso das arestas: "))
		arestas.append(aresta)

	grafo = create_tour(arestas)
	#print(grafo)
	drawing(grafo)

	#otimizando com o 3-opt
	new_tour = three_opt(arestas) #erro, debugar

	#mostrando
	grafo_otimizado = create_tour(new_tour)
	#print(grafo)
	drawing(grafo)
