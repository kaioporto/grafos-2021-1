# grafos-2021-1

Algoritmos relacionados a disciplina de Teoria dos Grafos da UFBA. 2021.1

## Algoritmo de Otimização 3-opt

O 3-opt tem o objetivo de melhorar uma rota já escolhida pelo algoritmo do TSP. Visto que o TSP não resulta sempre no melhor caminho.

O algoritmo do 3-opt estuda melhores possibilidades através de troca de conexões para que o caminho do ciclo fique menos custoso. Ele escolhe três arestas aleatórias e as permutam de local, sem alterar o ciclo fechado, de maneira que o custo total da rota seja menor que o inicial. Fazendo esse processo pra várias arestas do ciclo, encontramos um caminho menos custoso para o grafo inteiro. 

## Libs necessárias

* networkx
* matplotlib

- É possível instalar as duas através do pip, como o comando:
	pip install networkx
	pip install matplotlib


### Matplotlib

Lib para visualização de gráficos que vai nos ajudar a enxergar os grafos gerados

### Networkx
Lib utilizada para estudo e simulação de redes, que nos oferece uma iterface voltada para grafos bem completa e útil. Vamos usar para fazer nossas operações nos grafos, antes de mostrálos

